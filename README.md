# Docker Debian Stretch - RVM - Rails - Sessionizer

This repository is used for building a custom Docker image for [Minnestar's Sessionizer App](https://github.com/minnestar/sessionizer).

## Name of This Docker Image
[registry.gitlab.com/jhsu802701s/docker-debian-stretch-rvm-rails-sessionizer](https://gitlab.com/jhsu802701/docker-debian-stretch-rvm-rails-sessionizer/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-stretch-min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-stretch-min-rvm/container_registry)

## What's Added
* The latest version of Ruby
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems and Ruby used in the Sessionizer App
* Bundler
* The mailcatcher gem

## Things NOT Included
This Docker image does not include all versions of Ruby, Rails, pg, nokogiri, and ffi.  Instead, I have custom Docker images for every Rails app I'm working on.

## What's the Point?
This Docker image is used for developing the Sessionizer app.  The process of getting started is MUCH faster in a development environment that comes with the correct versions of Ruby and the correct versions of certain gems already pre-installed.  The rails, pg, nokogiri, and ffi gems take a long time to install.

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
